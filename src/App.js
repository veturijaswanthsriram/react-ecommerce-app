import { Route, Routes } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Home from "./pages/Home";
import Product from "./pages/Product";

function App() {
	return (
		<>
			<Header className="header" />
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/product/:productId" element={<Product />} />
			</Routes>
			<Footer />
		</>
	);
}

export default App;
