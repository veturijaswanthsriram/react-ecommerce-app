import { Grid, Typography } from "@mui/material";
import React from "react";
import CustomCard from "../../custom/CustomCard";

const products = [
	{
		id: 1,
		chipLabel: "25% off",
		title: "Catch Italian Seasoning Grinder",
		offerPrice: "225.00",
		originalPrice: "250.00",
		imageSrc:
			"https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F10.CatchItalianSeasoningGrinder.png&w=3840&q=75",
		rating: 4,
	},
	{
		id: 2,
		chipLabel: "25% off",
		title: "Catch Italian Seasoning Grinder",
		offerPrice: "225.00",
		originalPrice: "250.00",
		imageSrc:
			"https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F10.CatchItalianSeasoningGrinder.png&w=3840&q=75",
		rating: 4,
	},
	{
		id: 3,
		chipLabel: "25% off",
		title: "Catch Italian Seasoning Grinder",
		offerPrice: "225.00",
		originalPrice: "250.00",
		imageSrc:
			"https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F10.CatchItalianSeasoningGrinder.png&w=3840&q=75",
		rating: 4,
	},
	{
		id: 4,
		chipLabel: "25% off",
		title: "Catch Italian Seasoning Grinder",
		offerPrice: "225.00",
		originalPrice: "250.00",
		imageSrc:
			"https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F10.CatchItalianSeasoningGrinder.png&w=3840&q=75",
		rating: 4,
	},
];

const RelatedProducts = () => {
	return (
		<>
			<Typography
				fontSize="20px"
				fontWeight="700"
				marginBottom="1.5rem"
				marginTop="3rem"
			>
				RelatedProducts
			</Typography>

			<Grid container spacing={3}>
				{products.map((product) => (
					<Grid key={product.id} item xs={12} sm={6} md={4} lg={3}>
						<CustomCard item={product} />
					</Grid>
				))}
			</Grid>
		</>
	);
};

export default RelatedProducts;
