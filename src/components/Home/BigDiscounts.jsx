import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import CustomCarousel from "../../custom/CustomCarousel";
import CustomImage from "../../custom/CustomImage";
import SectionHeader from "./SectionHeader";

const items = [
	{
		id: 1,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 2,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 3,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 4,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 5,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 6,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 7,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 8,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
	{
		id: 9,
		src: "./images/big-discount/big-discount-1.png",
		title: "Tony TV 1080p",
		discountedPrice: "278",
		originalPrice: "278",
	},
];

const BigDiscountsCarouselItem = ({ item }) => {
	return (
		<Box
			padding="1rem"
			backgroundColor="common.white"
			borderRadius="0.5rem"
			gap="0.5rem"
			display="flex"
			flexDirection="column"
			key={item.id}
		>
			<CustomImage src={item.src} />
			<Typography fontWeight="600">{item.title}</Typography>
			<Box display="flex" gap="0.5rem" alignItems="center">
				<Typography fontWeight="600" color="error">
					${item.discountedPrice}
				</Typography>
				<Typography fontWeight="600" color="#7D879C">
					<s>${item.originalPrice}</s>
				</Typography>
			</Box>
		</Box>
	);
};

const breakPoints = [
	{
		width: 1,
		itemsToShow: 2,
	},
	{
		width: 650,
		itemsToShow: 4,
	},
	{
		width: 950,
		itemsToShow: 6,
	},
];

const BigDiscounts = () => {
	return (
		<section style={{ margin: "4.5rem 0 0 0" }}>
			<SectionHeader title="Big Discounts" />
			<Box position="relative">
				<CustomCarousel items={items} breakPoints={breakPoints}>
					<BigDiscountsCarouselItem />
				</CustomCarousel>
			</Box>
		</section>
	);
};

export default BigDiscounts;
