import { Box, Grid } from "@mui/material";
import React from "react";
import CustomCard from "../../custom/CustomCard";
import SectionHeader from "./SectionHeader";

const moreForYouItems = [
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},

	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},

	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
	{
		chipLabel: "10% off",
		title: "Tarz T3",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/flash/flash-1.png",
		rating: 4,
	},
];

const MoreForYou = () => {
	return (
		<section style={{ margin: "4.5rem 0 0 0" }}>
			<SectionHeader title="More For You" />
			<Box position="relative">
				<Grid container spacing={3}>
					{moreForYouItems.map((item, index) => (
						<Grid
							key={index}
							index={index}
							item
							xs={12}
							sm={6}
							md={4}
							lg={3}
						>
							<CustomCard item={item} />
						</Grid>
					))}
				</Grid>
			</Box>
		</section>
	);
};

export default MoreForYou;
