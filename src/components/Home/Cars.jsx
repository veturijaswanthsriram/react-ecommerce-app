import React from "react";
import CustomProductSection from "../../custom/CustomProductSection";

const sidebarItems = [
	{
		id: 1,
		src: "./images/ferrari.webp",
		title: "Ferrari",
	},
	{
		id: 2,
		src: "./images/ferrari.webp",
		title: "Tesla",
	},
	{
		id: 3,
		src: "./images/ferrari.webp",
		title: "BMW",
	},
	{
		id: 4,
		src: "./images/ferrari.webp",
		title: "Toyota",
	},
	{
		id: 5,
		src: "./images/ferrari.webp",
		title: "Mini",
	},
	{
		id: 6,
		src: "./images/ferrari.webp",
		title: "Ford",
	},
];

const data = [
	{
		id: 1,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/cars/car-1.png",
		rating: 4,
	},
	{
		id: 2,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/cars/car-1.png",
		rating: 4,
	},
	{
		id: 3,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/cars/car-1.png",
		rating: 4,
	},
	{
		id: 4,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/cars/car-1.png",
		rating: 4,
	},
	{
		id: 5,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/cars/car-1.png",
		rating: 4,
	},
	{
		id: 6,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/cars/car-1.png",
		rating: 4,
	},
];

const Cars = () => {
	return (
		<CustomProductSection
			title="Cars"
			sidebarItems={sidebarItems}
			items={data}
		/>
	);
};

export default Cars;
