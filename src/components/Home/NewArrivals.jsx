import { Card, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import CustomImage from "../../custom/CustomImage";
import SectionHeader from "./SectionHeader";

const items = [
	{
		src: "./images/new-arrival/new-arrival-1.png",
		title: "Sunglass",
		price: "150",
	},
	{
		src: "./images/new-arrival/new-arrival-2.png",
		title: "Makeup",
		price: "250",
	},
	{
		src: "./images/new-arrival/new-arrival-3.png",
		title: "Smart Watch",
		price: "350",
	},
	{
		src: "./images/new-arrival/new-arrival-4.png",
		title: "Lipstick",
		price: "15",
	},
	{
		src: "./images/new-arrival/new-arrival-5.png",
		title: "Green Plant",
		price: "55",
	},
	{
		src: "./images/new-arrival/new-arrival-6.png",
		title: "Bonsai Plant",
		price: "535",
	},
];

const NewArrivals = () => {
	return (
		<section style={{ margin: "4.5rem 0 0 0" }}>
			<SectionHeader title="New Arrivals" />
			<Box padding="1rem" backgroundColor="common.white">
				<Grid container spacing={3}>
					{items.map((item, index) => (
						<Grid
							key={index}
							item
							xs={6}
							sm={4}
							md={3}
							lg={2}
							sx={{
								fontWeight: "600",
							}}
						>
							<Card
								sx={{
									display: "flex",
									flexDirection: "column",
									gap: "0.5rem",
									boxShadow: "0",
								}}
							>
								<CustomImage src={item.src} />
								<Typography variant="span">
									{item.title}
								</Typography>
								<Typography color="error" variant="span">
									${item.price}
								</Typography>
							</Card>
						</Grid>
					))}
				</Grid>
			</Box>
		</section>
	);
};

export default NewArrivals;
