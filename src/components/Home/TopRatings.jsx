import { Card, CardContent, Grid, Rating, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import CustomImage from "../../custom/CustomImage";
import SectionHeader from "./SectionHeader";

const topRatingItems = [
	{
		id: 1,
		imageSrc: "./images/top-rating-1.png",
		rating: 4,
		totalRatings: 49,
		title: "Camera",
		price: "3,300",
	},
	{
		id: 2,
		imageSrc: "./images/top-rating-1.png",
		rating: 5,
		totalRatings: 20,
		title: "Shoe",
		price: "400",
	},
	{
		id: 3,
		imageSrc: "./images/top-rating-1.png",
		rating: 5,
		totalRatings: 65,
		title: "Phone",
		price: "999",
	},
	{
		id: 4,
		imageSrc: "./images/top-rating-1.png",
		rating: 5,
		totalRatings: 75,
		title: "Watch",
		price: "999",
	},
];

const TopRatings = () => {
	return (
		<section className="top-ratings" style={{ margin: "3rem 0 0 0" }}>
			<SectionHeader title={"Top Ratings"} />
			<Box
				borderRadius="1rem"
				padding="1rem"
				backgroundColor="common.white"
			>
				<Grid container spacing={2}>
					{topRatingItems.map((item) => (
						<Grid
							key={item.id}
							xs={6}
							md={3}
							item
							sx={{ fontWeight: "600" }}
						>
							<Card
								sx={{
									display: "flex",
									alignItems: "center",
									flexDirection: "column",
									justifyContent: "center",
									borderRadius: "0.5rem",
									boxShadow: "0",
								}}
							>
								<CustomImage src={item.imageSrc} />
								<CardContent
									sx={{
										display: "flex",
										alignItems: "center",
										flexDirection: "column",
										gap: "0.35rem",
									}}
								>
									<Box
										display="flex"
										alignItems="center"
										gap="0.25rem"
									>
										<Rating
											value={item.rating}
											readOnly
											size="small"
										/>
										<Typography
											variant="span"
											fontSize="12px"
										>
											({item.totalRatings})
										</Typography>
									</Box>
									<Typography variant="span">
										{item.title}
									</Typography>
									<Typography color="error" variant="span">
										${item.price}
									</Typography>
								</CardContent>
							</Card>
						</Grid>
					))}
				</Grid>
			</Box>
		</section>
	);
};

export default TopRatings;
