import React from "react";
import CustomProductSection from "../../custom/CustomProductSection";

const sidebarItems = [
	{
		id: 1,
		src: "./images/ferrari.webp",
		title: "Apple",
	},
	{
		id: 2,
		src: "./images/ferrari.webp",
		title: "Dell",
	},
	{
		id: 3,
		src: "./images/ferrari.webp",
		title: "Xiaomi",
	},
	{
		id: 4,
		src: "./images/ferrari.webp",
		title: "Asus",
	},
	{
		id: 5,
		src: "./images/ferrari.webp",
		title: "Sony",
	},
	{
		id: 6,
		src: "./images/ferrari.webp",
		title: "Acer",
	},
];

const data = [
	{
		id: 1,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/mobile-phones/mobile-phone-1.png",
		rating: 4,
	},
	{
		id: 2,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/mobile-phones/mobile-phone-1.png",
		rating: 4,
	},
	{
		id: 3,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/mobile-phones/mobile-phone-1.png",
		rating: 4,
	},
	{
		id: 4,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/mobile-phones/mobile-phone-1.png",
		rating: 4,
	},
	{
		id: 5,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/mobile-phones/mobile-phone-1.png",
		rating: 4,
	},
	{
		id: 6,
		chipLabel: "25% off",
		title: "NikeCourt Zoom Vapor Cage",
		offerPrice: "187.50",
		originalPrice: "250.00",
		imageSrc: "./images/mobile-phones/mobile-phone-1.png",
		rating: 4,
	},
];

const MobilePhones = () => {
	return (
		<CustomProductSection
			title="Mobile Phones"
			sidebarItems={sidebarItems}
			items={data}
		/>
	);
};

export default MobilePhones;
