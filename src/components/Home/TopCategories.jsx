import { Box, Chip } from "@mui/material";
import CategoryIcon from "@mui/icons-material/Category";
import React from "react";
import CustomCarousel from "../../custom/CustomCarousel";
import SectionHeader from "./SectionHeader";
import CustomImage from "../../custom/CustomImage";

const topCategoryItems = [
	{
		id: 1,
		category: "Headphone",
		orders: "3k orders this week",
		imageSrc: "./images/category/category-1.png",
	},
	{
		id: 2,
		category: "Watch",
		orders: "3k orders this week",
		imageSrc: "./images/category/category-2.png",
	},
	{
		id: 3,
		category: "Sunglass",
		orders: "3k orders this week",
		imageSrc: "./images/category/category-3.png",
	},
	{
		id: 4,
		category: "Headphone",
		orders: "3k orders this week",
		imageSrc: "./images/category/category-1.png",
	},
];

const TopCategoryCarouselItem = ({ item }) => {
	return (
		<Box
			padding="1rem"
			backgroundColor="common.white"
			borderRadius="0.5rem"
		>
			<Box position="relative">
				<CustomImage src={item.imageSrc} />
				<Box
					display="flex"
					justifyContent="space-around"
					sx={{
						position: "absolute",
						top: "1rem",
						width: "100%",
						zIndex: 10,
					}}
				>
					<Chip
						size="small"
						color="secondary"
						sx={{ fontSize: "10px", fontWeight: "600" }}
						label={item.category}
					/>
					<Chip
						size="small"
						sx={{ fontSize: "10px", fontWeight: "600" }}
						label={item.orders}
					/>
				</Box>
			</Box>
		</Box>
	);
};

const breakPoints = [
	{
		width: 1,
		itemsToShow: 1,
	},
	{
		width: 650,
		itemsToShow: 2,
	},
	{
		width: 950,
		itemsToShow: 3,
	},
	{ width: 1300, itemsToShow: 4 },
];

const TopCategories = () => {
	return (
		<section className="flash-deals" style={{ margin: "4.5rem 0 0 0" }}>
			<SectionHeader title="Top Categories" Icon={CategoryIcon} />
			<Box position="relative">
				<CustomCarousel
					items={topCategoryItems}
					breakPoints={breakPoints}
				>
					<TopCategoryCarouselItem />
				</CustomCarousel>
			</Box>
		</section>
	);
};

export default TopCategories;
