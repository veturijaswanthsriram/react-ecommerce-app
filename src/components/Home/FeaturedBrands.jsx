import { Grid, Typography } from "@mui/material";
import { Box } from "@mui/material";
import React from "react";
import CustomImage from "../../custom/CustomImage";
import SectionHeader from "./SectionHeader";

const featuredBrandItems = [
	{
		id: 1,
		imageSrc: "./images/featured-brand-1.png",
		title: "London Britches",
	},
	{
		id: 2,
		imageSrc: "./images/featured-brand-1.png",
		title: "Jim & Jago",
	},
];

const FeaturedBrands = () => {
	return (
		<section className="top-ratings" style={{ margin: "3rem 0 0 0" }}>
			<SectionHeader title={"Featured Brands"} />
			<Box
				borderRadius="1rem"
				padding="1rem"
				backgroundColor="common.white"
			>
				<Grid container spacing={3}>
					{featuredBrandItems.map((item) => (
						<Grid
							key={item.id}
							xs={6}
							item
							sx={{
								fontWeight: "600",
								display: "flex",
								flexDirection: "column",
								gap: "0.5rem",
							}}
						>
							<CustomImage src={item.imageSrc} />
							<Typography
								sx={{ color: "#2B3445" }}
								variant="span"
							>
								{item.title}
							</Typography>
						</Grid>
					))}
				</Grid>
			</Box>
		</section>
	);
};

export default FeaturedBrands;
