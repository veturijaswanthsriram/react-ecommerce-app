import { SearchOutlined } from "@mui/icons-material";
import LocalPhoneSharpIcon from "@mui/icons-material/LocalPhoneSharp";
import MailSharpIcon from "@mui/icons-material/MailSharp";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import {
	IconButton,
	Menu,
	MenuItem,
	TextField,
	Button,
	Box,
	Container,
	Badge,
	Typography,
	Link,
} from "@mui/material";
import React, { useState } from "react";
import { styled } from "@mui/system";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";

const categories = [
	"All Categories",
	"Car",
	"Clothes",
	"Electronics",
	"Laptop",
	"Desktop",
	"Camera",
	"Toys",
];

const SearchBar = styled(TextField)(({ theme }) => ({
	width: "100%",
	"& .Mui-FormControl-root": {
		width: "100%",
	},
	"& .MuiOutlinedInput-root": {
		display: "flex",
		alignItems: "center",
		gap: "0.5rem",
		flex: 1,
		borderRadius: 100,
		padding: "0 0 0 1rem",
	},
	"& .MuiOutlinedInput-input": {
		padding: "0",
	},
	"& .MuiButton-root": {
		borderTopRightRadius: 100,
		borderBottomRightRadius: 100,
		padding: "0.75rem 1.5rem",
		whiteSpace: "nowrap",
		minWidth: "auto",
		height: "100%",
		borderLeft: "1px solid rgb(218, 225, 231)",
		backgroundColor: theme.palette.primary.main,
		color: theme.palette.text.primary,
	},
}));

const CustomMenu = ({ items, fontSize, size }) => {
	const [anchorEl, setAnchorEl] = useState(null);
	const [selectedIndex, setSelectedIndex] = useState(0);
	const open = Boolean(anchorEl);

	const handleButtonClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleMenuItemClick = (event, index) => {
		setSelectedIndex(index);
		setAnchorEl(null);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	return (
		<>
			<Button
				onClick={handleButtonClick}
				sx={{
					fontSize: fontSize,
					minWidth: "auto",
					fontWeight: size === "small" ? "600" : "400",
					textTransform: "capitalize",
				}}
			>
				{items[selectedIndex]}{" "}
				<KeyboardArrowDownIcon
					fontSize={size === "small" ? "small" : "medium"}
				/>
			</Button>
			<Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
				{items.map((category, index) => (
					<MenuItem
						key={index}
						onClick={(e) => handleMenuItemClick(e, index)}
						selected={index === selectedIndex}
						sx={{ fontSize: fontSize }}
					>
						{category}
					</MenuItem>
				))}
			</Menu>
		</>
	);
};

const Header = () => {
	return (
		<header
			className="header"
			style={{
				display: "flex",
				flexDirection: "column",
			}}
		>
			<Box backgroundColor="secondary.main">
				<Container
					sx={{
						maxWidth: "1300px",
						height: "40px",
						display: "flex",
						justifyContent: "space-between",
						alignItems: "center",
						color: "text.secondary",
					}}
					maxWidth={false}
				>
					<Box display="flex" gap="1rem">
						<Box
							gap="0.5rem"
							sx={{ display: { xs: "none", md: "flex" } }}
						>
							<LocalPhoneSharpIcon fontSize="small" />
							<Typography fontSize="12px">
								+88012 3456 7894
							</Typography>
						</Box>
						<Box
							gap="0.5rem"
							sx={{ display: { xs: "none", md: "flex" } }}
						>
							<MailSharpIcon fontSize="small" />
							<Typography fontSize="12px">
								support@ui-lib.com
							</Typography>
						</Box>
						<Box
							component="img"
							src="./images/logo.svg"
							sx={{ display: { xs: "block", md: "none" } }}
							height="28px"
						/>
					</Box>
					<Box display="flex" gap="1.5rem" alignItems="center">
						<Link
							fontSize="12px"
							sx={{
								display: {
									xs: "none",
									md: "block",
									textDecoration: "none",
								},
							}}
						>
							Theme FAQ's
						</Link>
						<Link
							fontSize="12px"
							sx={{
								display: {
									xs: "none",
									md: "block",
									textDecoration: "none",
								},
							}}
						>
							Need Help?
						</Link>
						<CustomMenu
							items={["EN", "BN", "HN"]}
							fontSize="12px"
							size="small"
						/>
						<CustomMenu
							items={["USD", "EUR", "BDT", "INR"]}
							fontSize="12px"
							size="small"
						/>
					</Box>
				</Container>
			</Box>
			<Box backgroundColor="common.white" boxShadow={3}>
				<Container
					sx={{
						maxWidth: "1300px",
						display: "flex",
						alignItems: "center",
						height: "80px",
						justifyContent: { xs: "center", md: "space-between" },
					}}
					maxWidth={false}
				>
					<Box
						sx={{
							display: { xs: "none", md: "flex" },
						}}
						gap="1rem"
						alignItems="center"
					>
						<img
							style={{ height: "1.75rem" }}
							src="https://bazar-react.vercel.app/assets/images/logo2.svg"
							alt="bazar"
						/>
						{/* <Button
                            variant="contained"
                            color="primary"
                            sx={{
                                padding: "0.25rem 0.15rem",
                            }}
                        >
                            <CategoryIcon fontSize="small" />
                            <KeyboardArrowDownIcon fontSize="small" />
                        </Button> */}
					</Box>
					<Box sx={{ maxWidth: "650px", width: "100%" }}>
						<SearchBar
							color="error"
							placeholder="Searching for..."
							className="search-bar"
							variant="outlined"
							InputProps={{
								startAdornment: (
									<SearchOutlined
										fontSize="small"
										sx={{
											color: "#A4AAB4",
										}}
									/>
								),
								endAdornment: <CustomMenu items={categories} />,
							}}
						/>
					</Box>
					<Box sx={{ display: { xs: "none", md: "block" } }}>
						<Box display="flex" gap="1rem">
							<IconButton
								sx={{ backgroundColor: "rgb(243, 245, 249)" }}
							>
								<PersonOutlineIcon />
							</IconButton>
							<Badge badgeContent={3} color="error">
								<IconButton
									sx={{
										backgroundColor: "rgb(243, 245, 249)",
									}}
								>
									<ShoppingBagOutlinedIcon />
								</IconButton>
							</Badge>
						</Box>
					</Box>
				</Container>
			</Box>
		</header>
	);
};

export default Header;
