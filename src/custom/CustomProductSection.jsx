import { Grid, Box, Paper, Typography } from "@mui/material";
import React from "react";
import SectionHeader from "../components/Home/SectionHeader";
import CustomCard from "../custom/CustomCard";

const Sidebar = ({ items }) => {
	return (
		<Paper
			elevation={1}
			sx={{
				boxSizing: "border-box",
				backgroundColor: "common.white",
				padding: "1rem",
				borderRadius: "0.5rem",
				transition: "all 250ms ease-in-out",
				minWidth: "240px",
				height: "100%",
				display: { xs: "none", md: "block" },
			}}
		>
			<Box
				display="flex"
				alignItems="stretch"
				flexDirection="column"
				gap="1rem"
			>
				{items.map((item, index) => (
					<Paper
						key={index}
						sx={{
							padding: "0.5rem 1rem",
							display: "flex",
							gap: "10px",
							alignItems: "center",
							backgroundColor: "primary.main",
							borderRadius: "0.25rem",
							transition: "all 250ms ease-in-out",
							":hover": {
								boxShadow: 3,
							},
						}}
					>
						<Box
							component="img"
							src={item.src}
							width="20px"
							height="20px"
							sx={{
								objectFit: "cover",
							}}
						/>
						<Typography fontSize="17px" fontWeight="600">
							{item.title}
						</Typography>
					</Paper>
				))}
			</Box>
			<Paper
				elevation={0}
				sx={{
					marginTop: "3rem",
					padding: "0.5rem 1rem",
					borderRadius: "0.25rem",
					transition: "all 250ms ease-in-out",
					":hover": {
						boxShadow: 3,
					},
				}}
			>
				<Typography fontSize="17px" fontWeight="600">
					View Brands
				</Typography>
			</Paper>
		</Paper>
	);
};

const CustomProductSection = ({ title, sidebarItems, items }) => {
	return (
		<section style={{ margin: "4.5rem 0 0 0" }}>
			<Box display="flex" gap="2rem">
				<Sidebar items={sidebarItems} />
				<Box display="flex" flexDirection="column" width="100%">
					<SectionHeader title={title} />
					<Grid container spacing={3}>
						{items.map((item) => (
							<Grid key={item.id} item xs={12} sm={6} lg={4}>
								<CustomCard item={item} />
							</Grid>
						))}
					</Grid>
				</Box>
			</Box>
		</section>
	);
};

export default CustomProductSection;
