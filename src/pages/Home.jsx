import { Container, Grid } from "@mui/material";
import Banner from "../components/Home/Banner";
import BigDiscounts from "../components/Home/BigDiscounts";
import BrandBanners from "../components/Home/BrandBanners";
import Cars from "../components/Home/Cars";
import Categories from "../components/Home/Categories";
import FeaturedBrands from "../components/Home/FeaturedBrands";
import FlashDeals from "../components/Home/FlashDeals";
import MobilePhones from "../components/Home/MobilePhones";
import MoreForYou from "../components/Home/MoreForYou";
import NewArrivals from "../components/Home/NewArrivals";
import Optics from "../components/Home/Optics";
import Services from "../components/Home/Services";
import TopCategories from "../components/Home/TopCategories";
import TopRatings from "../components/Home/TopRatings";

const Home = () => {
	return (
		<>
			<Banner />
			<main className="main">
				<Container sx={{ maxWidth: "1300px" }} maxWidth={false}>
					<FlashDeals />
					<TopCategories />
					<Grid container spacing={3}>
						<Grid item xs={12} xl={6}>
							<TopRatings />
						</Grid>
						<Grid item xs={12} lg={6}>
							<FeaturedBrands />
						</Grid>
					</Grid>
					<NewArrivals />
					<BigDiscounts />
					<Cars />
					<MobilePhones />
					<BrandBanners />
					<Optics />
					<Categories />
					<MoreForYou />
					<Services />
				</Container>
			</main>
		</>
	);
};

export default Home;
